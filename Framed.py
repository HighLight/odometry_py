import cv2 as cv
import numpy as np
import imutils

#Открываем видео 
video = cv.VideoCapture("test_video/IMG_3492_Trim_10.mp4")  

# Или Веб камеру
# video = cv2.VideoCapture(0, cv2.CAP_DSHOW)

# Если что-то не так говоим гг
if not video.isOpened():
    print('Ошибка при открытии видео') #Error while opening the video

# устанавливаем счетчик
counter=0

# запускаем бесконечный цикл и проверяем статус камеры
while video.isOpened():
   
    # читаем кадры с записи или камеры в frame
    _, frame = video.read()
   
    # переводим кадр в черно-белую градацию
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # создаем функцию классификатор и сохраняем ее в переменной
    #face_cascade = cv2.CascadeClassifier("путь/haarcascade_frontalface_default.xml")
    # вызываем метод с параметром в виде кадра gray
    # метод вернет список найднных областей
    #faces = face_cascade.detectMultiScale(gray)
    #cv.imwrite("./detected_from_video_screen/frame_%d.jpg" %counter, frame)
    cv.imwrite("framed/frame_{0}.jpg".format(counter), frame)
    counter = counter+1
    cv.imshow("frame", frame)
    if cv.waitKey(1) == 27:
      break


cap.release()
cv.destroyAllWindows()

