function [orientation, camera_pose, M] = ransacP3P_pos_cal_parallel(matched_keypoints1_idx, ...
                                                                    landmarks, keypoints, sub_frame, K, ...
                                                                    num_p3p_RANSAC_iterations, ...
                                                                    p3p_RANSAC_inliers_threshold, ...
                                                                    parallel_pool)
% query_keypoints should be 2x1000
% all_matches should be 1x1000 and correspond to the output from the
%   matchDescriptors() function from exercise 3.
% inlier_mask should be 1xnum_matched (!!!) and contain, only for the
%   matched keypoints (!!!), 0 if the match is an outlier, 1 otherwise.
keypoints = keypoints(sub_frame).corners(matched_keypoints1_idx,:)';
landmarks = landmarks(1:3,:);

num_iterations = num_p3p_RANSAC_iterations;
pixel_tolerance = p3p_RANSAC_inliers_threshold;
k = 3;

% Initialize RANSAC.
inlier_mask = zeros(1, size(keypoints, 2));
num_inliers = zeros(num_iterations,1);
idx_inliers = zeros(num_iterations,3);


% RANSAC
parfor i = 1:num_iterations
    % only sample from the original landmarks
    [landmark_sample, idx] = datasample(...
        landmarks, k, 2, 'Replace', false);
    keypoints_sample = keypoints(:, idx);
    
    normalized_bearings = K\[keypoints_sample; ones(1, 3)];
    for ii = 1:3
        normalized_bearings(:, ii) = normalized_bearings(:, ii) / ...
            norm(normalized_bearings(:, ii), 2);
    end
    poses = p3p(landmark_sample(1:3,:), normalized_bearings);
    R_C_W_guess = zeros(3, 3, 2);
    t_C_W_guess = zeros(3, 1, 2);
    for ii = 0:1
        R_W_C_ii = real(poses(:, (2+ii*4):(4+ii*4)));
        t_W_C_ii = real(poses(:, (1+ii*4)));
        R_C_W_guess(:,:,ii+1) = R_W_C_ii';
        t_C_W_guess(:,:,ii+1) = -R_W_C_ii'*t_W_C_ii;
    end

    % Count inliers:
    projected_points = projectPoints(...
        (R_C_W_guess(:,:,1) * landmarks) + ...
        repmat(t_C_W_guess(:,:,1), ...
        [1 size(landmarks, 2)]), K);
    
    difference = keypoints(1:2,:) - projected_points;
    errors = sum(difference.^2, 1);
    is_inlier = errors < pixel_tolerance^2;
    
    projected_points = projectPoints(...
        (R_C_W_guess(:,:,2) * landmarks) + ...
        repmat(t_C_W_guess(:,:,2), ...
        [1 size(landmarks, 2)]), K);
    difference = keypoints(1:2,:) - projected_points;
    errors = sum(difference.^2, 1);
    alternative_is_inlier = errors < pixel_tolerance^2;
    
    if nnz(alternative_is_inlier) > nnz(is_inlier)
        num_inliers(i) = nnz(alternative_is_inlier);
    else
        num_inliers(i) = nnz(is_inlier);
    end
    
    idx_inliers(i,:) = idx;
end

% end of parallelized RANSAC gather the information out of the parfor loop

[~, max_num_inliers_idx] = max(num_inliers);
max_idx = idx_inliers(max_num_inliers_idx,:);

% -----------------------------------------------------------------------
landmark_sample = landmarks(:,max_idx);
keypoints_sample = keypoints(:, max_idx);

normalized_bearings = K\[keypoints_sample; ones(1, 3)];
for ii = 1:3
    normalized_bearings(:, ii) = normalized_bearings(:, ii) / ...
        norm(normalized_bearings(:, ii), 2);
end
poses = p3p(landmark_sample(1:3,:), normalized_bearings);
R_C_W_guess = zeros(3, 3, 2);
t_C_W_guess = zeros(3, 1, 2);
for ii = 0:1
    R_W_C_ii = real(poses(:, (2+ii*4):(4+ii*4)));
    t_W_C_ii = real(poses(:, (1+ii*4)));
    R_C_W_guess(:,:,ii+1) = R_W_C_ii';
    t_C_W_guess(:,:,ii+1) = -R_W_C_ii'*t_W_C_ii;
end

% Count inliers:
projected_points = projectPoints(...
    (R_C_W_guess(:,:,1) * landmarks) + ...
    repmat(t_C_W_guess(:,:,1), ...
    [1 size(landmarks, 2)]), K);

difference = keypoints(1:2,:) - projected_points;
errors = sum(difference.^2, 1);
is_inlier = errors < pixel_tolerance^2;

projected_points = projectPoints(...
    (R_C_W_guess(:,:,2) * landmarks) + ...
    repmat(t_C_W_guess(:,:,2), ...
    [1 size(landmarks, 2)]), K);
difference = keypoints(1:2,:) - projected_points;
errors = sum(difference.^2, 1);
alternative_is_inlier = errors < pixel_tolerance^2;

if nnz(alternative_is_inlier) > nnz(is_inlier)
    orientation = R_C_W_guess(:,:,2)';
    camera_pose = -R_C_W_guess(:,:,2)'*t_C_W_guess(:,:,2);
    M = K*[R_C_W_guess(:,:,2), t_C_W_guess(:,:,2)];
else
    orientation = R_C_W_guess(:,:,1)';
    camera_pose = -R_C_W_guess(:,:,1)'*t_C_W_guess(:,:,1);
    M = K*[R_C_W_guess(:,:,1), t_C_W_guess(:,:,1)];
end


end

