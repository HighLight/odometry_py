% function to track keypoints in the new image
% ... to get new keypoints in the new image
% ... and to update keypoints from old images to the new image
function [keypoints, camera] = matchCorners(keypoints, ...
                                            img1, img2, ...
                                            last_frame_of_interest, frame, ...
                                            pointTracker, ...
                                            num_RANSAC_iterations, RANSAC_inliers_threshold, ...
                                            max_num_of_keypoints, min_quality_to_extract_features, ...
                                            min_uniqueness_for_corners, min_distance_for_existing_corners, ...
                                            parallel_pool, ...
                                            camera, newest_keyframe)
    % get corners of the preceeding frame
    corners1 = keypoints(frame-1).corners;
    release(pointTracker); % allow property value and input characteristics changes
    initialize(pointTracker,corners1,img1);
    % track in the new image the keypoints from the preceeding image
    [corners2, point_validity] = step(pointTracker, img2);

    matched_corners1 = corners1(point_validity,:);
    matched_corners2 = corners2(point_validity,:);
    matched_corners1 = [matched_corners1'; ones(1, size(matched_corners1,1))];
    matched_corners2 = [matched_corners2'; ones(1, size(matched_corners2,1))];

    % only perform 8 point RANSAC if there are at least 8 points
    if size(matched_corners1,2) >= 8
        % perform 8 point RANSAC
        if parallel_pool == true
            inliers_mask = ...
                ransac8point_parallel(matched_corners1, matched_corners2, ...
                                      num_RANSAC_iterations, RANSAC_inliers_threshold);
        else
            inliers_mask = ...
                ransac8point(matched_corners1, matched_corners2, ...
                             num_RANSAC_iterations, RANSAC_inliers_threshold);
        end

        % update matched_corners
        matched_corners1 = matched_corners1(:, inliers_mask);
        matched_corners2 = matched_corners2(:, inliers_mask);
        
        % update point_validity
        point_validity(point_validity == true) = inliers_mask;
    end
    
    for i = last_frame_of_interest:frame-2
        keypoints(i).corners = keypoints(i).corners(point_validity(1:size(keypoints(i).corners,1)),:);
    end
    if exist('camera','var') == 1 % landmarks have been calculated update the landmarked_keypoints_mask
        landmarks_idx = point_validity(1:size(camera(newest_keyframe).corners_idx,2));
        camera(frame).corners_idx = camera(newest_keyframe).corners_idx(landmarks_idx);
        landmarks_idx = landmarks_idx(camera(newest_keyframe).corners_idx);
        camera(frame).landmarks = camera(newest_keyframe).landmarks(:, landmarks_idx);
    end

    keypoints(frame-1).corners = matched_corners1(1:2,:)';
    % only get more keypoints if keypoints are below keypoints treshold
    if size(matched_corners2,2) < max_num_of_keypoints
        % get more keypoints in the newest image, this time, get all keypoints
        % that might be used (min quality is set to 0.01)
        keypoints2 = getCorners(img2, min_quality_to_extract_features, min_uniqueness_for_corners);
        keypoints2 = keypoints2.corners;
        dist_of_new_corners = pdist2(matched_corners2(1:2,:)', keypoints2);
        dist_of_new_corners = min(dist_of_new_corners,[],1);
        new_corners_mask = dist_of_new_corners > min_distance_for_existing_corners;
        keypoints2 = keypoints2(new_corners_mask,:);
        % put matched keypoints and new keypoints of img 2 into one vector
        allkeypoints2 = [matched_corners2(1:2,:)'; keypoints2];
        
        if size(allkeypoints2,1) < max_num_of_keypoints
            % if amount of new unique keypoints it below max_num_of_keypoints, add all
            keypoints(frame).corners = allkeypoints2;
        else
            % else just take the best and fill up to max_num_of_keypoints
            keypoints(frame).corners = allkeypoints2(1:max_num_of_keypoints,:);
        end
    else
        keypoints(frame).corners = matched_corners2(1:2,1:max_num_of_keypoints)';
    end
    if exist('camera','var') == 0 % no landmarks have been calculated yet, ignore camera
        camera = 0;
    end
end