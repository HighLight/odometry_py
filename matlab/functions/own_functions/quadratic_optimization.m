function camera_poses_optimized = quadratic_optimization(camera_poses, ...
                                                         camera_poses_optimized, ...
                                                         keyframes_memory)
% enough keyframes have been calculated,
% make a quadratic optimization with the last keyframes
keyframes_numbers = keyframes_memory(end-nnz(keyframes_memory)+1:end);
keyframes_poses = camera_poses([1 3],keyframes_numbers);

% fit in a quadratic curve within the last keyframe poses
p = polyfit(keyframes_poses(1,:),keyframes_poses(2,:),2);

% calculate the steepness of the quadratic curve near
% the position of the points
poses_x = camera_poses(1,keyframes_numbers(1):keyframes_numbers(end));
poses_y = camera_poses(3,keyframes_numbers(1):keyframes_numbers(end));
poses_x_lower = floor(100*poses_x)/100;
poses_x_upper =  ceil(100*poses_x)/100;
poses_y_lower = polyval(p,poses_x_lower);
poses_y_upper = polyval(p,poses_x_upper);

% normal line: y - pose_y = m * (x - pose_x)
%              y = m * x + (pose_y - m * pose_x) =  m * x + b
% calculate steepness of normal line (= -1/steepness_tangent)
steepness_normal = -(poses_x_upper-poses_x_lower)./(poses_y_upper-poses_y_lower);
% calculate y-intercept of normal line (= pose_y - m*pose_x
y_intercept = poses_y - poses_x .* steepness_normal;

% calculate intersection point between quadratic curve and normal line
% solving steepness_normal * x + y_intercept = p(1)*x^2 + p(2)*x + p(3)
%        0 = p(1) * x^2  +  (p(2)-steepness_normal) * x  +  (p(3)-y_intercept)
poses_x_1_2 = [(-(p(2)-steepness_normal) + sqrt((p(2)-steepness_normal).^2 - 4*p(1)*(p(3)-y_intercept))) / (2*p(1));
               (-(p(2)-steepness_normal) - sqrt((p(2)-steepness_normal).^2 - 4*p(1)*(p(3)-y_intercept))) / (2*p(1))];

% find the correct solution of this quadratic equation
% (the correct solution is the one close to the camera_pose)
[~, poses_x_opt_idx] = min(abs(poses_x_1_2-[poses_x;poses_x]));
poses_x_1_2 = poses_x_1_2(:);
poses_x_opt_idx = 2*([1:size(poses_x_opt_idx,2)]-1) + poses_x_opt_idx;
poses_x_opt = poses_x_1_2(poses_x_opt_idx,:)';
poses_y_opt = polyval(p,poses_x_opt);

% save the optimized poses
camera_poses_optimized(1,keyframes_numbers(1):keyframes_numbers(end)) = poses_x_opt;
camera_poses_optimized(3,keyframes_numbers(1):keyframes_numbers(end)) = poses_y_opt;
end