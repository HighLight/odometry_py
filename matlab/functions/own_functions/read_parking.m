function img = read_parking(parking_path, parking_subpath, parking_img_name, frame)
    img = rgb2gray(imread(fullfile(parking_path, parking_subpath, ...
                                   sprintf(parking_img_name, frame-1))));
end