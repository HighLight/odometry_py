function img = read_malaga(malaga_path, malaga_subpath, left_images, frame)
    img = rgb2gray(imread(fullfile(malaga_path, malaga_subpath, ...
                                   left_images(frame).name)));
end