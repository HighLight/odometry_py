function keypoints = getCorners(img, min_quality_to_extract_features, min_uniqueness_for_corners)
    % use detectMinEigenFeatures that uses the Shi-Tomasi algorithm
    corners = detectMinEigenFeatures(img, 'MinQuality', min_quality_to_extract_features);
    % corners = detectHarrisFeatures(img, 'MinQuality', min_quality_to_extract_features);
    corners = corners.Location;
    [keypoints.corners, ~,~] = uniquetol(corners,min_uniqueness_for_corners,'ByRows',true);
end