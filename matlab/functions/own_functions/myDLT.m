function [camera_orientation, camera_poses, M] = myDLT(matched_keypoints1_idx, landmarks, keypoints, sub_frame, K)
    matched_keypoints1 = keypoints(sub_frame).corners(matched_keypoints1_idx,:);
    

    M_C_W = estimatePoseDLT(...
        matched_keypoints1, ...
        landmarks(1:3,:)', K);
    
    M = K*M_C_W;
    
    R_C2_W = M_C_W(:, 1:3);
    T_C2_W = M_C_W(:, end);

    camera_orientation = R_C2_W';
    camera_poses = -R_C2_W'*T_C2_W;
end