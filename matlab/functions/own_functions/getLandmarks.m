function [p_W_landmarks, ...
                keypoints_idx, ...
                orientation_cam2_W, center_cam2_W, ...
                M2, ...
                p_W_average_depth] = getLandmarks(keypoints, ...
                                                  min_num_of_keypoints_for_landmarks, ...
                                                  K, first_frame, current_frame, ds, ...
                                                  min_bearing_angle, ...
                                                  camera)
% get all keypoints in current frame
all_keypoints2 = keypoints(current_frame).corners;
% only save the entries that have a matched keypoints in the first_frame
matched_keypoints2 = all_keypoints2(1:size(keypoints(first_frame).corners,1),:);
keypoints1 = [keypoints(first_frame).corners'; ones(1, size(keypoints(first_frame).corners,1))];
keypoints2 = [matched_keypoints2'; ones(1, size(keypoints(first_frame).corners,1))];
% the calculation of bearing angle has been dropped since the distance
% check yielded more stable results

if exist('camera','var')
    % calculate bearing vector for every keypoint
    % vector_world = R_W_C*(inv(K)*P_pixels) = R_C_W'*(K\P_pixels);
    vector1 = camera(first_frame).orientation*(K\keypoints1);
    vector2 = camera(current_frame).orientation*(K\keypoints2);
    % angle = acos( dot(x, y) / (norm(x)*norm(y)) ) using vectorized calculation for speed:
    angle = 180/pi*acos(   sum(vector1.*vector2,1)   ./   ...
        sqrt( sum(vector1.*vector1,1) .* sum(vector2.*vector2,1) )   );
    
    angle_sorted = sort(angle);
    % take the bearing angle that yields the 80 percent best corners
    bearing_angle_thresh_idx = round((1-0.8)*length(angle_sorted));
    bearing_angle_thresh_temp = angle_sorted(bearing_angle_thresh_idx);
    if bearing_angle_thresh_temp > min_bearing_angle
        bearing_angle_thresh = bearing_angle_thresh_temp;
    else
        bearing_angle_thresh = min_bearing_angle;
    end
    keypoints_idx = angle > bearing_angle_thresh; % bearing angle has to be larger than 2 degrees
else
    % don't consider keypoints that have a displacement below a certain threshold
    distance = keypoints1 - keypoints2;
    distance = sum(distance.*distance,1);
    if ds == 2
        keypoints_displacement_thresh = 400;
    elseif ds == 1
        keypoints_displacement_thresh = 100;
    else
        keypoints_displacement_thresh = 400;
    end
    keypoints_idx = distance > keypoints_displacement_thresh;
end
keypoints1 = keypoints1(:, keypoints_idx);
keypoints2 = keypoints2(:, keypoints_idx);
% only if there are enough keypoints to calculate enough landmarks for the
% next frames do SFM ...
if size(keypoints1,2) >= min_num_of_keypoints_for_landmarks
    % Estimate the essential matrix E using the 8-point algorithm
    E = estimateEssentialMatrix(keypoints1, keypoints2, K, K);

    % Obtain extrinsic parameters (R,t) from E
    [Rots,u3] = decomposeEssentialMatrix(E);
    
    % Disambiguate among the four possible configurations
    [R_C2_W,T_C2_W] = disambiguateRelativePose(Rots,u3,keypoints1,keypoints2,K,K);
    
    % Triangulate a point cloud using the final transformation (R,T)
    M1 = K * eye(3,4);
    M2 = K * [R_C2_W, T_C2_W];
    orientation_cam2_W = R_C2_W';
    center_cam2_W = -R_C2_W'*T_C2_W;
    landmarks = linearTriangulation(keypoints1,keypoints2,M1,M2);
    landmarks_infront = landmarks(3,:) > 0 & landmarks(3,:) < 25;
    landmarks = landmarks(:,landmarks_infront);
    keypoints_idx(keypoints_idx == true) = landmarks_infront;
    
    % calculate average depth that is used later to determine if landmarks
    % are good
    average_depth = sum(landmarks(3,:))/size(landmarks,2);
    
    p_W_landmarks = landmarks;
    p_W_average_depth = average_depth;
else
    % if no landmarks could be triangulated just set the values to 0
    p_W_landmarks = 0;
    p_W_average_depth = 0;
    orientation_cam2_W = 0;
    center_cam2_W = 0;
    M2 = eye(3,4);
end
end