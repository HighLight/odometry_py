function showKeypoints(VO_unmatched_keypoints, ...
                       VO_matched_keypoints, ...
                       VO_landmarked_keypoints, ...
                       VO_num_unmatched_keypoints, ...
                       VO_num_matched_keypoints, ...
                       VO_num_landmarked_keypoints, ...
                       keypoints, frame, ...
                       camera, newest_keyframe)
    num_of_camera_poses = length(VO_num_unmatched_keypoints.YData);
    num_of_VO = length(VO_unmatched_keypoints.XData);
    if exist('newest_keyframe','var') == 1 % landmarks have been triangulated already
        % number of landmarked keypoints:
        landmarked = size(camera(frame).landmarks,2);
        if landmarked > 1
            VO_num_landmarked_keypoints.YData = [landmarked, VO_num_landmarked_keypoints.YData(1:num_of_camera_poses-1)];
            landmarked_keypoints = keypoints(frame).corners(1:size(camera(frame).corners_idx,2),:);
            landmarked_keypoints = landmarked_keypoints(camera(frame).corners_idx,:);
            % display the landmarked_keypoints by updating XData and YData of
            % our keypoints-lineobject
            if landmarked > num_of_VO
                VO_landmarked_keypoints.XData = landmarked_keypoints(1:num_of_VO,1)';
                VO_landmarked_keypoints.YData = landmarked_keypoints(1:num_of_VO,2)';
            else
                VO_landmarked_keypoints.XData = [landmarked_keypoints(1:end,1)', landmarked_keypoints(end,1)'*ones(1,num_of_VO - landmarked)];
                VO_landmarked_keypoints.YData = [landmarked_keypoints(1:end,2)', landmarked_keypoints(end,2)'*ones(1,num_of_VO - landmarked)];
            end
        else
            % no landmarked keypoints available
            VO_landmarked_keypoints.XData = zeros(1, size(VO_landmarked_keypoints.XData,2));
            VO_landmarked_keypoints.YData = zeros(1, size(VO_landmarked_keypoints.YData,2));
        end
    end
    % number of matched keypoints
    matched = size(keypoints(frame-1).corners,1);
    VO_num_matched_keypoints.YData = [matched, VO_num_matched_keypoints.YData(1:num_of_camera_poses-1)];
    % number of unmatched keypoints
    unmatched = size(keypoints(frame).corners,1) - matched;
    VO_num_unmatched_keypoints.YData = [unmatched, VO_num_unmatched_keypoints.YData(1:num_of_camera_poses-1)];
    
    if unmatched > num_of_VO
        VO_unmatched_keypoints.XData = keypoints(frame).corners(matched+1:matched+num_of_VO,1)';
        VO_unmatched_keypoints.YData = keypoints(frame).corners(matched+1:matched+num_of_VO,2)';
    else
        VO_unmatched_keypoints.XData = [keypoints(frame).corners(matched+1:end,1)', keypoints(frame).corners(end,1)'*ones(1,num_of_VO - unmatched)];
        VO_unmatched_keypoints.YData = [keypoints(frame).corners(matched+1:end,2)', keypoints(frame).corners(end,2)'*ones(1,num_of_VO - unmatched)];
    end
    
    if matched > num_of_VO
        VO_matched_keypoints.XData = keypoints(frame).corners(1:num_of_VO,1)';
        VO_matched_keypoints.YData = keypoints(frame).corners(1:num_of_VO,2)';
    else
        VO_matched_keypoints.XData = [keypoints(frame).corners(1:matched,1)', keypoints(frame).corners(matched,1)'*ones(1,num_of_VO - matched)];
        VO_matched_keypoints.YData = [keypoints(frame).corners(1:matched,2)', keypoints(frame).corners(matched,2)'*ones(1,num_of_VO - matched)];
    end
end