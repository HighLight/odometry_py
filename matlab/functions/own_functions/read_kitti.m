function img = read_kitti(kitti_path, kitti_subpath, kitti_img_name, frame)
    img = imread(fullfile(kitti_path, kitti_subpath, ...
                          sprintf(kitti_img_name, frame-1)));
end