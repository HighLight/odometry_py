function camera_poses_optimized = linear_optimization(camera_poses, ...
                                                      camera_poses_optimized, ...
                                                      keyframes_memory, ...
                                                      first_frame)
% do a linear optimization
% optimization between the to last keyframes
keyframes_numbers = keyframes_memory(end-2:end-1) + first_frame-1;
points_x = camera_poses(1,keyframes_numbers(1):keyframes_numbers(2));
points_z = camera_poses(3,keyframes_numbers(1):keyframes_numbers(2));

% place the camera positions equally on the line
% using: x_new = x_new(1) + (x_new(end) - x_new(1)) * n/(number_of_points-1)
% for n = 0, 1, 2, ..., number_of_points-1
n = 0:keyframes_numbers(2)-keyframes_numbers(1);
points_x = points_x(1) + (points_x(end)-points_x(1)).*n/(size(n,2)-1);
points_z = points_z(1) + (points_z(end)-points_z(1)).*n/(size(n,2)-1);

% save the linear optimized poses
camera_poses_optimized(1,keyframes_numbers(1):keyframes_numbers(2)) = points_x;
camera_poses_optimized(3,keyframes_numbers(1):keyframes_numbers(2)) = points_z;
end                                                              