function inliers_mask = ...
        ransac8point_parallel(matched_corners1, matched_corners2, num_RANSAC_iterations, RANSAC_inliers_threshold)
    % initialize
    number_of_inliers = zeros(num_RANSAC_iterations,1);
    indexes_of_sample = zeros(num_RANSAC_iterations,8);
    
    % RANSAC
    for iteration = 1:num_RANSAC_iterations
        
        % choose k = 8 random points out of matched_keyoints
        [matched_keypoints_sample1, idx] = datasample(...
            matched_corners1, 8, 2, 'Replace', false);
        matched_keypoints2 = matched_corners2;
        
        % Estimate the fudamental matrix F using the 8-point algorithm
        F_est = fundamentalEightPoint_normalized(matched_keypoints_sample1, matched_keypoints2(:, idx));

        %calculate epipolar line of image1 by using F and the 2d points of image0
        epipolar_lines1 = F_est*matched_corners1;
        %calculate distance between 2d point of image1 and epipolar line of image1
        %   without using for-loop for increased speed
        %   uses hesse formula for a point (px,py) and line (a*x+b*y+c):
        %   distance = abs(px*a + py*b)/norm([a b]);
        distance = abs(sum(matched_corners2.*epipolar_lines1,1)) ./ ...
             sqrt(sum(epipolar_lines1(1:2,:).*epipolar_lines1(1:2,:),1));
        % slower:
        % distance = abs(diag(matched_corners2'*epipolar_lines1))' ./ ...
        %     sqrt(sum(epipolar_lines1(1:2,:).*epipolar_lines1(1:2,:),1));

        % calculate distance as error of p1'*F*p0
        % distance = abs(diag(matched_keypoints1'*F_est*matched_keypoints0));
        
        % count how many entries in distance have value < inliers_threshold
        number_of_inliers(iteration) = length(distance(distance < RANSAC_inliers_threshold));
        indexes_of_sample(iteration,:) = idx;
    end
    
    % get the iteration that yielded the most inliers
    [~, idx_of_most_inliers] = max(number_of_inliers);
    idx = indexes_of_sample(idx_of_most_inliers,:);
    % Estimate the fudamental matrix F using the 8-points of the iteration that yielded the most inliers
    F_est = fundamentalEightPoint_normalized(matched_corners1(:,idx), matched_corners2(:, idx));
    %calculate epipolar line of image1 by using F and the 2d points of image0
    epipolar_lines1 = F_est*matched_corners1;
    distance = abs(diag(matched_corners2'*epipolar_lines1))' ./ ...
            sqrt(sum(epipolar_lines1(1:2,:).*epipolar_lines1(1:2,:),1));
    %save inliers mask
    inliers_mask = distance < RANSAC_inliers_threshold;
end