function inliers_mask = ...
        ransac8point(matched_corners1, matched_corners2, num_RANSAC_iterations, RANSAC_inliers_threshold)

    % initialize max_number_of_inliers
    max_number_of_inliers = 0;
    
    % RANSAC
    for iteration = 1:num_RANSAC_iterations
        % choose k = 8 random points out of matched_keyoints
        [matched_keypoints_sample1, idx] = datasample(...
            matched_corners1, 8, 2, 'Replace', false);
        
        % Estimate the fudamental matrix F using the 8-point algorithm
        F_est = fundamentalEightPoint_normalized(matched_keypoints_sample1, matched_corners2(:, idx));

        %calculate epipolar line of image1 by using F and the 2d points of image0
        epipolar_lines1 = F_est*matched_corners1;
        %calculate distance between 2d point of image1 and epipolar line of image1
        %   without using for-loop for increased speed
        %   uses hesse formula for a point (px,py) and line (a*x+b*y+c):
        %   distance = abs(px*a + py*b)/norm([a b]);
        distance = abs(diag(matched_corners2'*epipolar_lines1))' ./ ...
            sqrt(sum(epipolar_lines1(1:2,:).*epipolar_lines1(1:2,:),1));

        % calculate distance as error of p1'*F*p0
        % distance = abs(diag(matched_keypoints1'*F_est*matched_keypoints0));
        
        % count how many entries in distance have value < inliers_threshold
        number_of_inliers = length(distance(distance < RANSAC_inliers_threshold));

        if number_of_inliers > max_number_of_inliers
            %my_ransac(iteration) = number_of_inliers;
            %save inliers mask
            inliers_mask = distance < RANSAC_inliers_threshold;
            %update max_number_of_inliers
            max_number_of_inliers = number_of_inliers;
        end
    end
end