close all
clear all
clc

%% Setup %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ds = 0; % 0: KITTI, 1: Malaga, 2: parking

first_frame = 1; % choose frame to begin with (mainly used for debugging, set to 1 for standard operation)

% Set paths that contain the datasets -----------------------------------
kitti_path = fullfile(pwd, 'kitti');
malaga_path = fullfile(pwd, 'malaga');
parking_path = fullfile(pwd, 'parking');

% add paths for functions -----------------------------------------------
addpath('functions/own_functions');
addpath('functions/exercise_solns');

%% GATHER CAMERA INTRINSICS AND FULL PATH OF INPUT FILES %%%%%%%%%%%%%%%%
% for convenience, the image reading has been put into functions, in case
% reading needs to be modified, the modification will affect the whole code
if ds == 0
    % need to set kitti_path to folder containing "00" and "poses"
    assert(exist('kitti_path', 'var') ~= 0);
    kitti_subpath = '00/image_0';
    kitti_img_name = '%06d.png';
    img1 = read_kitti(kitti_path, kitti_subpath, kitti_img_name, first_frame);
    img2 = read_kitti(kitti_path, kitti_subpath, kitti_img_name, first_frame+1);
    last_frame = 4540;
    K = [718.856   0     607.1928
           0     718.856 185.2157
           0       0       1];
       
       %{
elseif ds == 1
    % Path containing the many files of Malaga 7.
    assert(exist('malaga_path', 'var') ~= 0);
    malaga_subpath = 'malaga-urban-dataset-extract-07_rectified_800x600_Images';
    % Acquisition of directories for left images has been changed, since depending on
    % folder content, invalid files might have appeared in past
    left_images = dir(fullfile(malaga_path, malaga_subpath, '*left*.jpg'));

    img1 = read_malaga(malaga_path, malaga_subpath, left_images, first_frame);
    img2 = read_malaga(malaga_path, malaga_subpath, left_images, first_frame+1);
    last_frame = length(left_images);
    K = [621.18428   0       404.0076
           0       621.18428 309.05989
           0         0         1];
elseif ds == 2
    % Path containing images, depths and all...
    assert(exist('parking_path', 'var') ~= 0);
    parking_subpath = 'images';
    parking_img_name = 'img_%05d.png';
    img1 = read_parking(parking_path, parking_subpath, parking_img_name, first_frame);
    img2 = read_parking(parking_path, parking_subpath, parking_img_name, first_frame+1);
    last_frame = 598;
    K = [331.37   0     320
           0    369.568 240
           0      0       1];
else
    assert(false);
           %}
end
           
           

%% TUNE FOR PERFORMANCE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fix the seed of the random number generator for reproducible results
rng(1);
% choose if RANSAC iterations should run on parallel pool 
parallel_pool = true;

% 8-POINT RANSAC
% num of ransac iterations to triangulate new landmarks
num_RANSAC_iterations = 1500;
% threshold for inliers in 8-point RANSAC algorithm
% (distance from image point to epipolar line in px)
RANSAC_inliers_threshold = 1;

% P3P + P3P RANSAC
% num of ransac iterations to calculate new positions if landmarks exist
num_p3p_RANSAC_iterations = 500;
% threshold for inliers in RANSAC P3P algorithm
% (distance of reprojected point in px)
p3p_RANSAC_inliers_threshold = 10;

% KEYPOINTS EXTRACTION
% max number of keypoints to be extracted at every frame
max_num_of_keypoints = 3000; % 1500 % 1000 % 500
if ds == 0
    % the higher the minimum quality, the less corners get extracted, higher performance
    min_quality_to_extract_features = 0.005; % 0.01 % 0.012; % 0.02
    % threshold for uniqueness of corners (in order to enhance stability,
    % corners that are too close to each other won't get considered)
    min_uniqueness_for_corners = 0.008; % 0.01 % 0.04; % 0.05
    %{
elseif ds == 1
    min_quality_to_extract_features =  0.005;
    min_uniqueness_for_corners = 0.008;
else
    min_quality_to_extract_features =  0.006;
    min_uniqueness_for_corners = 0.05;
    %}
end
% bearing angle is the angle between two vectors showing from the beginning
% and the end of a keypoint-track to the associated landmark.
% (keypoint-track = a series containing the same corner tracked in several
%  consecutive frames)
% we don't want to consider keypoint-tracks that have a insufficient
% bearing angle
% The best 80% of corners are taken that have the largest bearing angle
% though, a min_bearing_angle is still enforced (critical for hard terrain
% such as bushes)
min_bearing_angle = 0.75; % [degrees]
% min_distance of new corners in px (for getting new corners, we don't want to
% add corners that exist already. a new corner is considered not existing yet, if
% there is no existing corner in the min_distance area)
min_distance_for_existing_corners = 10; % [px]

if ds == 0
    % min num of keypoints needed to calculate landmarks during initialization
    min_num_of_keypoints_for_landmarks = 120;
else
    min_num_of_keypoints_for_landmarks = 60;
end

%% CREATE A PARALLEL POOL FOR RANSAC ITERATIONS %%%%%%%%%%%%%%%%%%%%%%%%%
if parallel_pool == true
    % get the current available pool
    current_pool = gcp('nocreate');
    % get the number of available CPUs on machine
    num_of_threads = feature('numCores');
    pool_message = 'Creating parallel pool for running RANSAC parallelized ... this might take a few seconds';
    % if a pool exists ...
    if length(gcp('nocreate')) == 1
        % check if the number of workers are as desired
        if current_pool.NumWorkers ~= num_of_threads
            % if this is not given, delete the old pool ...
            delete(gcp('nocreate'));
            % ... and create a new pool
            disp(pool_message);
            p = parpool(num_of_threads);
        end
    else
        % create a new pool anyway, since no pool exists
        disp(pool_message);
        p = parpool(num_of_threads);
    end
end

%% CREATE THE VISUAL ODOMETRY INTERFACE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[main_fig, ...
    VO_landmarks, ...
    VO_path, ...
    VO_ax_path, ...
    VO_fullpath, ...
    VO_img, ...
    VO_unmatched_keypoints, ...
    VO_matched_keypoints, ...
    VO_landmarked_keypoints, ...
    VO_num_unmatched_keypoints, ...
    VO_num_matched_keypoints, ...
    VO_num_landmarked_keypoints] = plotVO(img1, last_frame, max_num_of_keypoints, first_frame);
% make sure that window appears
pause(0);
should_create_video = false; % choose if pipeline should be recorded
if should_create_video
    % CREATE VIDEO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    video_path = 'E:/VO_video.avi';
    VidObj = VideoWriter(video_path,'Motion JPEG AVI'); % Prepare video file
    VidObj.FrameRate = 15;
    open(VidObj);
    writeVideo(VidObj, getframe(main_fig));
    % CREATE VIDEO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

%% MATCH KEYPOINTS: INITIALITATION WITH FIRST TWO FRAMES %%%%%%%%%%%%%%%%
%preallocate
keypoints(1:last_frame) = struct('corners',0); % 'corners' saves only corners that still can be tracked to newest image
% get the keypoints of the first image
keypoints(first_frame) = getCorners(img1, min_quality_to_extract_features, min_uniqueness_for_corners);
last_frame_of_interest = first_frame; % initialize the last frame number we are interested in using for position estimation
frame = first_frame + 1; % actual frame
% create pointTracker object that tracks points over frames
pointTracker = vision.PointTracker('NumPyramidLevels',4);
% track the keypoints of the first frame in the second frame and add new keypoints
[keypoints, ~] = matchCorners(keypoints, ...
                              img1, img2, ...
                              last_frame_of_interest, frame, ...
                              pointTracker, ...
                              num_RANSAC_iterations, RANSAC_inliers_threshold, ...
                              max_num_of_keypoints, min_quality_to_extract_features, ...
                              min_uniqueness_for_corners, min_distance_for_existing_corners, ...
                              parallel_pool);
% update image and visualize keypoints
VO_img.CData = img2;
VO_img.title.String = ['Frame ' int2str(frame) ' Being Processed'];
showKeypoints(VO_unmatched_keypoints, ...
              VO_matched_keypoints, ...
              VO_landmarked_keypoints, ...
              VO_num_unmatched_keypoints, ...
              VO_num_matched_keypoints, ...
              VO_num_landmarked_keypoints, ...
              keypoints, frame);
                   
% make sure updates are visible to the user
pause(0);
if should_create_video
    writeVideo(VidObj, getframe(main_fig));
end
%% CONTINUOUS OPERATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % 1st STEP: TRACK KEYPOINTS AND TRY TO INITIALIZE LANDMARKS
    % 2nd STEP: TRACK KEYPOINTS AND TRY TO CALCULATE NEW LANDMARKS
% initialize orientation and position of baseframe
% (baseframe = first frame that is used for structure from motion during
%              triangulation of landmarks)
baseframe_orientation = eye(3);
baseframe_pose = zeros(3,1);
is_initializing = true;
is_first_initialization = true;
for i = first_frame+2:last_frame % i stands for the current frame processed
    % display the current frame processed
    VO_img.title.String = ['Frame ' int2str(i) ' Being Processed'];
    % read in the current frame
    if ds == 0
        img1 = img2;
        img2 = read_kitti(kitti_path, kitti_subpath, kitti_img_name, i);
    elseif ds == 1
        img1 = img2;
        img2 = read_malaga(malaga_path, malaga_subpath, left_images, i);
    elseif ds == 2
        img1 = img2;
        img2 = read_parking(parking_path, parking_subpath, parking_img_name, i);
    else
        assert(false);
    end
    
    % -------------------------------------------------------------------
    % track keypoints in new image and get new keypoints
    if is_initializing % no landmarks have been calculated yet
        % we are in 1st STEP (TRACK KEYPOINTS AND TRY TO INITIALIZE LANDMARKS) -------------------------------------------
        % track keypoints ...
        [keypoints,      ~] = matchCorners(keypoints, ...
                                           img1, img2, ...
                                           last_frame_of_interest, i, ...
                                           pointTracker, ...
                                           num_RANSAC_iterations, RANSAC_inliers_threshold, ...
                                           max_num_of_keypoints, min_quality_to_extract_features, ...
                                           min_uniqueness_for_corners, min_distance_for_existing_corners, ...
                                           parallel_pool);
    else
        % we are in 2nd STEP (TRACK KEYPOINTS AND TRY TO TRIANGULATE NEW LANDMARKS) -----------------------------
        % track keypoints and also update the landmarks ...
        [keypoints, camera] = matchCorners(keypoints, ...
                                           img1, img2, ...
                                           last_frame_of_interest, i, ...
                                           pointTracker, ...
                                           num_RANSAC_iterations, RANSAC_inliers_threshold, ...
                                           max_num_of_keypoints, min_quality_to_extract_features, ...
                                           min_uniqueness_for_corners, min_distance_for_existing_corners, ...
                                           parallel_pool, ...
                                           camera, i-1);

        % calculate position of newest frame
        if size(camera(i).landmarks,2) >= 3
            if ds ~= 2 % if this is not parking dataset ...
                % calculate current position using P3P
                if parallel_pool
                    [camera(i).orientation, ...
                        camera_poses(:,i), ...
                        camera(i).M_C_W] = ransacP3P_pos_cal_parallel(camera(i).corners_idx, ...
                                                                      camera(i).landmarks, ...
                                                                      keypoints, i, K, ...
                                                                      num_p3p_RANSAC_iterations, ...
                                                                      p3p_RANSAC_inliers_threshold);
                else
                    [camera(i).orientation, ...
                        camera_poses(:,i), ...
                        camera(i).M_C_W] = ransacP3P_pos_cal(camera(i).corners_idx, ...
                                                             camera(i).landmarks, ...
                                                             keypoints, i, K, ...
                                                             num_p3p_RANSAC_iterations, ...
                                                             p3p_RANSAC_inliers_threshold);
                end
            
            else % use DLT for parking dataset (has been found to be more stable on parking dataset)
                [camera(i).orientation, ...
                    camera_poses(:,i), ...
                    camera(i).M_C_W] = myDLT(camera(i).corners_idx, ...
                                             camera(i).landmarks, ...
                                             keypoints, i, K);
            end
            
            % we need to transform the new camera_pose from
            % baseframe coordinate frame into world coordinate frame
            camera(i).orientation = baseframe_orientation*camera(i).orientation;
            camera_poses(:,i) = baseframe_orientation*camera_poses(:,i) + baseframe_pose;
            camera_poses_optimized(:,i) = camera_poses(:,i);
        else
            % sadly, no new position can be calculated, all that can be done
            % is redo initialitation
            [keypoints,      ~] = matchCorners(keypoints, ...
                                           img1, img2, ...
                                           last_frame_of_interest, i, ...
                                           pointTracker, ...
                                           num_RANSAC_iterations, RANSAC_inliers_threshold, ...
                                           max_num_of_keypoints, min_quality_to_extract_features, ...
                                           min_uniqueness_for_corners, min_distance_for_existing_corners, ...
                                           parallel_pool);
            % copy last position and orientation
            camera(i).orientation = camera(i-1).orientation;
            camera_poses(:,i) = camera_poses(:,i-1);
            camera_poses_optimized(:,i) = camera_poses(:,i);
            is_initializing = true;
        end
    end
    
    % TRY TO FIND A NEW KEYFRAME IN ORDER TO TRIANGULATE NEW LANDMARKS
    % calculate landmarks and their average depth ----------------
    if i - last_frame_of_interest >= 4 % make sure there is some distance between the two frames
        % intialize our temporary memory for triangulating new landmarks
        clear ransac_landmarks
        ransac_landmarks(1:i-last_frame_of_interest) = struct('landmarks',0, ...
                                                              'corners_idx',0, ...
                                                              'orientation_cam2',eye(3), ...
                                                              'center_cam2',zeros(3,1), ...
                                                              'M_C_W', zeros(3,4));
        average_depth = zeros(1,i-last_frame_of_interest);
        sufficient_landmarks = false(1,i-last_frame_of_interest);
        reasonable_landmarks_found = false;
        for j = last_frame_of_interest:i-1
            ransac_landmarks_idx = j-last_frame_of_interest+1; 
            % if we are still initializing, no camera orientations are available, 
            % bearing angle can't be calculated yet, check displacement of corners in image
            if is_initializing
                [ransac_landmarks(ransac_landmarks_idx).landmarks, ...
                    ransac_landmarks(ransac_landmarks_idx).corners_idx, ...
                    ransac_landmarks(ransac_landmarks_idx).orientation_cam2, ...
                    ransac_landmarks(ransac_landmarks_idx).center_cam2, ...
                    ransac_landmarks(ransac_landmarks_idx).M_C_W, ...
                    average_depth(ransac_landmarks_idx)] = getLandmarks(keypoints, ...
                                                                        min_num_of_keypoints_for_landmarks, ...
                                                                        K, j, i, ds, ...
                                                                        min_bearing_angle);
            else
                % camera position exist, calculate bearing angle
                [ransac_landmarks(ransac_landmarks_idx).landmarks, ...
                    ransac_landmarks(ransac_landmarks_idx).corners_idx, ...
                    ransac_landmarks(ransac_landmarks_idx).orientation_cam2, ...
                    ransac_landmarks(ransac_landmarks_idx).center_cam2, ...
                    ransac_landmarks(ransac_landmarks_idx).M_C_W, ...
                    average_depth(ransac_landmarks_idx)] = getLandmarks(keypoints, ...
                                                                        min_num_of_keypoints_for_landmarks, ...
                                                                        K, j, i, ds, ...
                                                                        min_bearing_angle, ...
                                                                        camera);
            end
            
            % check if enough landmarks exist
            if ds == 0 && exist('camera','var')
                % in the kitty dataset, if old landmarks exist, also demand
                % that the amount of new landmarks has to be larger than
                % the amount of current landmarks
                sufficient_landmarks(ransac_landmarks_idx) = ... 
                    size(ransac_landmarks(ransac_landmarks_idx).landmarks,2) > min_num_of_keypoints_for_landmarks & ...
                    size(ransac_landmarks(ransac_landmarks_idx).landmarks,2) > size(camera(i).landmarks,2);
            else
                sufficient_landmarks(ransac_landmarks_idx) = ... 
                    size(ransac_landmarks(ransac_landmarks_idx).landmarks,2) > min_num_of_keypoints_for_landmarks;
            end
            
            % if sufficient landmarks have been found, don't waste the
            % users time, break the for loop already
            if 4 > abs(8-average_depth(ransac_landmarks_idx)) && sufficient_landmarks(ransac_landmarks_idx) == true
                % it has been found that good landmarks are between 4 and 12
                % please let us know if better values are known
                reasonable_landmarks_found = true;
                break
            end
        end
        % check if reasonable landmarks have been found --------------
        if reasonable_landmarks_found % if a frame with good landmarks exist ...
            % our last_frame_of_interest is the frame that yielded the
            % best landmarks calculation, update that value
            last_frame_of_interest = last_frame_of_interest + ransac_landmarks_idx - 1;
            
            newest_keyframe = i; % set the current frame as the newest keyframe

            if ~exist('camera','var')
                % initialize camera pos and orientation and some
                % keyframes_memory that is used for linear optimization later on
                camera(1:last_frame) = struct('orientation',eye(3), 'landmarks',0, 'corners_idx', false, 'M_C_W', K*eye(3,4));
                camera_poses = zeros(3,last_frame);
                keyframes_memory = zeros(1,20);
                keyframes_memory(end) = last_frame_of_interest - first_frame + 1;
                is_initializing = false;
            end
            % add the newest keyframe number into the memory
            keyframes_memory = [keyframes_memory(2:end), newest_keyframe - first_frame + 1];
            
            % update the baseframe position and orientation to the position
            % and orientation of the frame that yielded the best landmarks
            baseframe_orientation = camera(last_frame_of_interest).orientation;
            baseframe_pose = camera_poses(:,last_frame_of_interest);
                
            % gather the camera pose and orientation from the best calculation
            camera_poses(:,newest_keyframe) = ransac_landmarks(ransac_landmarks_idx).center_cam2;
            camera(newest_keyframe).orientation = ransac_landmarks(ransac_landmarks_idx).orientation_cam2;
            camera(newest_keyframe).M_C_W = ransac_landmarks(ransac_landmarks_idx).M_C_W;

            % save the landmarks and corners_idx from the best calculation
            old_landmarks = camera(newest_keyframe).landmarks;
            old_corners_idx = camera(newest_keyframe).corners_idx;
            camera(newest_keyframe).landmarks = ransac_landmarks(ransac_landmarks_idx).landmarks;
            camera(newest_keyframe).corners_idx = ransac_landmarks(ransac_landmarks_idx).corners_idx;
            
            if length(old_landmarks) > 3 % if old landmarks exist ...
                % calculate the relative scale_factor
                new_corners_idx = camera(newest_keyframe).corners_idx;
                new_landmarks = camera(newest_keyframe).landmarks;
                old_landmarks_indexed = zeros(4,size(old_corners_idx,2));
                old_landmarks_indexed(:,old_corners_idx) = old_landmarks;
                new_landmarks_indexed = zeros(4,size(new_corners_idx,2));
                new_landmarks_indexed(:,new_corners_idx) = new_landmarks;
                if size(new_corners_idx,2) > size(old_corners_idx,2)
                    new_corners_idx = new_corners_idx(1,1:size(old_corners_idx,2));
                else
                    old_corners_idx = old_corners_idx(1,1:size(new_corners_idx,2));
                end
                common_old_landmarks = old_landmarks_indexed(:,old_corners_idx & new_corners_idx);
                common_new_landmarks = new_landmarks_indexed(:,old_corners_idx & new_corners_idx);
                
                % calculate all distances between all landmarks
                dist_old_landmarks = pdist2(common_old_landmarks', common_old_landmarks');
                dist_new_landmarks = pdist2(common_new_landmarks', common_new_landmarks');

                % calculate ratio between all distances of old and new landmarks
                ratio_landmarks = dist_old_landmarks ./ dist_new_landmarks;
                ratio_landmarks = ratio_landmarks(:);
                ratio_landmarks(isnan(ratio_landmarks)) = [];
                
                scale_factor = median(ratio_landmarks);
                % in order to decrease scale drift:
                % assume continuous motion, scale factor has to be in a
                % certain range to be physical
                % assume a car can apply more force in breaking ...
                if scale_factor < 0.3
                    scale_factor = 0.3;
                end
                % ... as in accelerating
                if scale_factor > 1.005
                    scale_factor = 1.005;
                end
                
                % scale the new calculations
                camera(newest_keyframe).landmarks(1:3,:) = scale_factor * camera(newest_keyframe).landmarks(1:3,:);
                camera_poses(:,newest_keyframe) = scale_factor * camera_poses(:,newest_keyframe);
                camera(newest_keyframe).M_C_W(:,4) = scale_factor * camera(newest_keyframe).M_C_W(:,4);
                
                % we need to transform the new camera_pose from
                % baseframe coordinate frame into world coordinate frame
                camera(newest_keyframe).orientation = baseframe_orientation*camera(newest_keyframe).orientation;
                camera_poses(:,newest_keyframe) = baseframe_orientation*camera_poses(:,newest_keyframe) + baseframe_pose;
                camera_poses_optimized(:, newest_keyframe) = camera_poses(:,newest_keyframe);
            end
           
            if is_first_initialization
                % no landmarks exist from a previous initialization
                % use DLT for performance, to calculate the remaining positions (since no
                % RANSAC is needed)
                for sub_frame = last_frame_of_interest:newest_keyframe
                    [camera(sub_frame).orientation, ...
                        camera_poses(:,sub_frame), ...
                        camera(sub_frame).M_C_W] = myDLT(camera(newest_keyframe).corners_idx, ...
                                                           camera(newest_keyframe).landmarks, ...
                                                           keypoints, sub_frame, K);
                end
                if ~exist('scale_factor','var')
                    % no scale_factor has been calculated yet, it's the very first SFM
                    % initialize some stuff for optimization
                    camera_poses_optimized = camera_poses;
                end
                is_first_initialization = false; % landmarks exist now
            else
                % it's not the first initialization anymore, do a linear
                % optimization
                camera_poses_optimized = linear_optimization(camera_poses, ...
                                                             camera_poses_optimized, ...
                                                             keyframes_memory, ...
                                                             first_frame);
                % a quadratic optimization has been implemented and dropped
                % since it yielded bad results
                % a cubic or even a polynomial of 4th order is needed but
                % was not implemented, since it has shown to be
                % computationally too expensive
                % if nnz(keyframes_memory) >= 5
                %     camera_poses_optimized = quadratic_optimization(camera_poses, ...
                %                                                     camera_poses_optimized, ...
                %                                                     keyframes_memory);
                % end
            end

            % we are not interested anymore in older frames than the current keyframe
            if last_frame_of_interest <= 3
                last_frame_to_delete = last_frame_of_interest;
            else
                last_frame_to_delete = last_frame_of_interest - 3;
            end
            % delete old entries to free memory
            for frame_idx = last_frame_to_delete : newest_keyframe - 3
                keypoints(frame_idx).corners = [];
                camera(frame_idx).orientation = [];
                camera(frame_idx).landmarks = [];
                camera(frame_idx).corners_idx = [];
                camera(frame_idx).M_C_W = [];
            end
            
            % set our last_frame_of_interest to the newest_keyframe
            last_frame_of_interest = newest_keyframe;
        end
    end
    % update image to the user
    VO_img.CData = img2;
    % visualize the Corners
    if ~exist('camera', 'var')
        % if no landmarks exist yet, we can't display corners with an
        % associated landmark
        showKeypoints(VO_unmatched_keypoints, ...
                      VO_matched_keypoints, ...
                      VO_landmarked_keypoints, ...
                      VO_num_unmatched_keypoints, ...
                      VO_num_matched_keypoints, ...
                      VO_num_landmarked_keypoints, ...
                      keypoints, i);
    else
        % landmarks exist already, also display corners with an associated
        % landmark
        showKeypoints(VO_unmatched_keypoints, ...
                      VO_matched_keypoints, ...
                      VO_landmarked_keypoints, ...
                      VO_num_unmatched_keypoints, ...
                      VO_num_matched_keypoints, ...
                      VO_num_landmarked_keypoints, ...
                      keypoints, i, ...
                      camera, newest_keyframe);
                  
        % sometimes, the camera position can yield a NaN value, that causes
        % the VO pipeline to crash. if this case appears just copy the
        % camera position of the last frame (usually this appears if almost
        % no landmarks can be triangulated)
        if isnan(camera_poses_optimized(1,i)) || isnan(camera_poses_optimized(2,i)) || isnan(camera_poses_optimized(3,i))
            idx = i;
            while isnan(camera_poses_optimized(1,idx)) || isnan(camera_poses_optimized(2,idx)) || isnan(camera_poses_optimized(3,idx))
                idx = idx - 1;
                camera_poses_optimized(:,i) = camera_poses_optimized(:,idx);
            end
        end
        % visualize the camera path of the last few frames
        % calculate an ax_path_border to set the limits of the path plot
        if i >= length(VO_path.XData)
            VO_path.XData = camera_poses_optimized(1,i-length(VO_path.XData)+1:i)';
            VO_path.YData = camera_poses_optimized(3,i-length(VO_path.XData)+1:i)';
            ax_path_border = sqrt((VO_path.XData(1)-VO_path.XData(end))^2 + ...
                                  (VO_path.YData(1)-VO_path.YData(end))^2);
        else
            VO_path.XData = camera_poses_optimized(1,1:length(VO_path.XData))';
            VO_path.YData = camera_poses_optimized(3,1:length(VO_path.XData))';
            ax_path_border = sqrt((VO_path.XData(1)-VO_path.XData(i))^2 + ...
                                  (VO_path.YData(1)-VO_path.YData(i))^2);
        end
        % set the axes limits accordingly:
        % the current camera position should be in the middle of the plot
        % in order to show a meaningful view, we set the ax_path_border to the
        % distance between the current camera position and the oldest position displayed
        % this allows a "zoom-effect" if positions are close to each other
        % (for example in a curve)
        VO_ax_path.XLim = [camera_poses_optimized(1,i)-ax_path_border, camera_poses_optimized(1,i)+ax_path_border];
        VO_ax_path.YLim = [camera_poses_optimized(3,i)-ax_path_border, camera_poses_optimized(3,i)+ax_path_border];

        % only visualize if landmarks exist ...
        landmarks = camera(i).landmarks;
        landmarks_size = size(landmarks,2);
        if landmarks_size > 1
            % ... transform landmarks into world coordinate frame ...
            landmarks = baseframe_orientation*landmarks(1:3,:) + ...
                        baseframe_pose*ones(1,size(landmarks,2));
            % ... and visualize them
            VO_landmarks_size = length(VO_landmarks.XData);
            if landmarks_size > VO_landmarks_size
                VO_landmarks.XData = landmarks(1,1:VO_landmarks_size)';
                VO_landmarks.YData = landmarks(3,1:VO_landmarks_size)';
            else
                VO_landmarks.XData = [landmarks(1,:)'; zeros(VO_landmarks_size - size(landmarks,2),1)];
                VO_landmarks.YData = [landmarks(3,:)'; zeros(VO_landmarks_size - size(landmarks,2),1)];
            end
        end
        % visualize the full camera path
        VO_fullpath.XData = camera_poses(1,:)';
        VO_fullpath.YData = camera_poses(3,:)';
    end
    % make sure visualizations are visible to the user
    pause(0);
    if should_create_video
        writeVideo(VidObj, getframe(main_fig));
    end
end
if should_create_video
    close(VidObj); % close file, terminate writing movie to disk
end