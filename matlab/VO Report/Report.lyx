#LyX file created by tex2lyx 2.0.7
\lyxformat 413
\begin_document
\begin_header
\textclass report
\begin_preamble
% IDSC LaTeX Thesis Template
% 
% Author(s):	Eric MÃ¼ller
% 				Institute for Dynamic Systems and Control
% 				Swiss Federal Institute of Technology (ETH) Zurich
% 
% Created:		2004/04/02  (Eric Mueller)
% 
% Notes: Has been tested on Windows 7 + MikTeX + TeXnicCenter
%
% Revisions: 	2009/05/29  (Soren Ebbesen)
% 				    2011/03/22	(Soren Ebbesen)
%             2013/03/08	(Soren Ebbesen)
%             2014/03/13	(Soren Ebbesen)
% ______________________________________________________________________________



\usepackage[english,st]{ethidsc}% Special IDSC styles and commands      	
								 % {german}/english: language of headings, etc.
								 % {st}/bt/mt: {semester}/bachelor/master thesis
							
% Page header (don't change)____________________________________________________
                 % Disable parindent
\rhead[\nouppercase{\rightmark}]{\thepage}  % Special headings
\lhead[\thepage]{\nouppercase{\leftmark}}   % Special headings
\cfoot{}                                    % Special headings


% Title page (please fill in)___________________________________________________
\title{\LaTeX\ Thesis Template v.1.4}


\studentA{Hans Muster}
\ethidA{97-906-739}
\semesterA{5}
\emailA{muster@student.ethz.ch}

%\studentB{Second Student}
%\ethidB{12-345-678}
%\semesterB{9}
%\emailB{second@student.ethz.ch}

\supervision{First Supervisor\\ Prof. Dr. Second Supervisor}
\date{March 2011}

\identification{IDSC-XX-YY-ZZ} 		% Project identifier

\infopage
\declaration

% Begin document________________________________________________________________
\chapter{Introduction}\label{sec:introduction}
This template is meant to be used for semester, bachelor, and master theses written at the Institute for Dynamic Systems and Control (IDSC), ETH Zurich. The template includes several examples of equations, figures, tables, etc. in order to act as a {\it very} short introduction to \TeX\! and \LaTeX. Yet, the template is also provided to ensure that all written work at IDSC shares identical formatting.

\section{The Preamble}\label{sec:preamble}
The preamble of the \LaTeX\! template defines the font size, page layout, language, report type, title, and author(s) of the report. The preamble of the current template is shown below. It should be more or less clear how you need to modify the preamble to fit your needs; if not, consult your supervisor.
%\lstset{language=TeX,numbers=none}
%\begin{lstlisting}[frame=lines]
\begin{verbatim}
 

 \usepackage[german,st]{ethidsc}% IDSC style
                                             % {german}/english: language
                                             % {st}/bt/mt: thesis type

 % Page header (don't change)
                  % Disable parindent
 \rhead[\nouppercase{\rightmark}]{\thepage}  % Special headings
 \lhead[\thepage]{\nouppercase{\leftmark}}   % Special headings
 \cfoot{}                                    % Special headings

 % Title page (please fill in)
 \title{\LaTeX\ Thesis Template v.1.4}       % Report title

 \studentA{Hans Muster}
 \ethidA{97-906-739}
 \semesterA{5}
 \emailA{muster@student.ethz.ch}

 % \studentB{Second Student}
 % \ethidB{12-345-678}
 % \semesterB{9}
 % \emailB{second@student.ethz.ch}

 \supervision{First Supervisor\\ Prof. Dr. Second Supervisor}
 \date{March 2011}

 \identification{IDSC-XX-YY-ZZ}              % Project identifier

 \infopage
 \declaration
 \end{verbatim}
%\end{lstlisting}

The style \texttt{ethidsc.sty} enforces certain changes to the original \texttt{report} class, e.g., the title page. The style accepts two options. The first option lets you choose the language of your report, i.e., the language of the title page, headings, info-page, etc. Valid options are: \texttt{german} (default) and \texttt{english}. The second option defines the type of report which will be printed on the title and info page. Valid options are: \texttt{st} (default), \texttt{bt}, and \texttt{mt} for semester, bachelor and master thesis, respectively. For instance, if you will be writing a master thesis in English, use
\begin{verbatim}
 \usepackage[english,mt]{ethidsc}\end{verbatim}
The command \texttt{\textbackslash infopage} prints an information page at the end of the document which you must sign before handing in the report.


\end_preamble
\options fleqn
\use_default_options false
\language english
\language_package none
\inputencoding auto
\fontencoding default
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\paperfontsize 10
\spacing single
\use_hyperref 0
\papersize a4paper
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 0
\use_mathdots 0
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard


\begin_inset ERT
status collapsed

\begin_layout Standard


\backslash
maketitle
\end_layout

\end_inset

 
\begin_inset ERT
status collapsed

\begin_layout Standard

% Create title page
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\end_layout

\begin_layout Standard


\begin_inset ERT
status collapsed

\begin_layout Standard

% Preamble______________________________________________________________________
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\end_layout

\begin_layout Standard


\begin_inset ERT
status collapsed

\begin_layout Standard


\backslash
pagenumbering{roman}
\end_layout

\end_inset

 
\begin_inset ERT
status collapsed

\begin_layout Standard

% Begin roman page numbering (i,ii,...)
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\end_layout

\begin_layout Standard


\begin_inset CommandInset include
LatexCommand input
preview false
filename "chapters/preamble.lyx"

\end_inset


\end_layout

\begin_layout Standard


\begin_inset ERT
status collapsed

\begin_layout Standard

% Chapters______________________________________________________________________
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\end_layout

\begin_layout Standard


\begin_inset ERT
status collapsed

\begin_layout Standard


\backslash
pagestyle
\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Standard

{
\end_layout

\end_inset

fancy
\begin_inset ERT
status collapsed

\begin_layout Standard

}
\end_layout

\end_inset

 
\begin_inset ERT
status collapsed

\begin_layout Standard

% Fancy headings
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Standard


\backslash
pagenumbering{arabic}
\end_layout

\end_inset

 
\begin_inset ERT
status collapsed

\begin_layout Standard

% Begin arabic page numbering (1,2,...)
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\end_layout

\begin_layout Standard


\begin_inset CommandInset include
LatexCommand input
preview false
filename "chapters/introduction.lyx"

\end_inset

 
\begin_inset Newpage cleardoublepage
\end_inset


\begin_inset CommandInset include
LatexCommand input
preview false
filename "chapters/howto.lyx"

\end_inset

 
\begin_inset Newpage cleardoublepage
\end_inset


\end_layout

\begin_layout Standard
\start_of_appendix


\begin_inset ERT
status collapsed

\begin_layout Standard

%dummy comment inserted by tex2lyx to ensure that this paragraph is not empty
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\begin_inset CommandInset include
LatexCommand input
preview false
filename "chapters/appendix.lyx"

\end_inset


\end_layout

\begin_layout Standard


\begin_inset ERT
status collapsed

\begin_layout Standard

% Bibliography__________________________________________________________________
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Standard

% Literature (Additional references can be added to the .bib-file manually, or by using, for example, the free application JabRef). Compile in the following order: latex -bibtex -latex -latex
\end_layout

\begin_layout Standard


\end_layout

\end_inset


\end_layout

\begin_layout Standard

 
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "bibliography"
options "plain"

\end_inset


\end_layout

\end_body
\end_document
