% IDSC LaTeX Thesis Template
% 
% Author(s):	Eric Müller
% 				Institute for Dynamic Systems and Control
% 				Swiss Federal Institute of Technology (ETH) Zurich
% 
% Created:		2004/04/02  (Eric Mueller)
% 
% Notes: Has been tested on Windows 7 + MikTeX + TeXnicCenter
%
% Revisions: 	2009/05/29  (Soren Ebbesen)
% 				    2011/03/22	(Soren Ebbesen)
%             2013/03/08	(Soren Ebbesen)
%             2014/03/13	(Soren Ebbesen)
% ______________________________________________________________________________
\documentclass[10pt,twoside,a4paper,fleqn]{report}


\usepackage[english,bt]{ethidsc} % Special IDSC styles and commands      	
								 % {german}/english: language of headings, etc.
								 % {st}/bt/mt: {semester}/bachelor/master thesis
\usepackage{graphicx}
\usepackage{import}
\usepackage{epstopdf}
\usepackage{ifpdf}
\usepackage[final]{pdfpages}
\usepackage{lmodern}

							
% Page header (don't change)____________________________________________________
\setlength{\parindent}{0em}                 % Disable parindent
\rhead[\nouppercase{\rightmark}]{\thepage}  % Special headings
\lhead[\thepage]{\nouppercase{\leftmark}}   % Special headings
\cfoot{}                                    % Special headings


% Title page (please fill in)___________________________________________________
\title{Model of a Three-Way Catalytic Converter in Simulink}


\studentA{Sandro Losa}
\ethidA{09-154-345}
\semesterA{6}
\emailA{slosa@student.ethz.ch}

%\studentB{Second Student}
%\ethidB{12-345-678}
%\semesterB{9}
%\emailB{second@student.ethz.ch}

\supervision{Prof. Dr. Christopher Onder \\ Andreas Ritter}
\date{June 2015}

\identification{IDSC-CO-AR-02} 		% Project identifier

\infopage
\declaration

% Begin document________________________________________________________________
\begin{document}

\maketitle 							% Create title page


% Preamble______________________________________________________________________

\pagenumbering{roman} 				% Begin roman page numbering (i,ii,...)

\input{chapters/preamble}

% Chapters______________________________________________________________________

\pagestyle{fancy}               	% Fancy headings
\pagenumbering{arabic}				% Begin arabic page numbering (1,2,...)

\input{chapters/introduction}
\cleardoublepage
\input{chapters/theoreticalbackground}
\cleardoublepage
\input{chapters/derivationmathematicalmodel}
\cleardoublepage
\input{chapters/implementationMatlabSimulink}
\cleardoublepage
\input{chapters/results}
\cleardoublepage
\input{chapters/outlook}

% Appendix______________________________________________________________________

%\appendix
%\input{chapters/appendix}


% Bibliography__________________________________________________________________
% Literature (Additional references can be added to the .bib-file manually, or by using, for example, the free application JabRef). Compile in the following order: latex -bibtex -latex -latex

\bibliographystyle{plain}
\bibliography{bibliography}

\end{document}