\contentsline {chapter}{\numberline {1}Structure of Visual Odometry Pipeline}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Graphical Illustration of our VO Concept }{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Description of our Algorithm}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Rough Overview of our Code}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Data Structure}{5}{section.1.4}
\contentsline {chapter}{\numberline {2}Main File}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Creating a Parallel Pool for RANSAC (parallel pool part in main.m)}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Continuous Operation Part in main.m}{6}{section.2.2}
\contentsline {chapter}{\numberline {3}Description of the most Important Functions}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Creating the VO Interface (plotVO.m)}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}Get Corners (getCorners.m)}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}Match Corners (matchCorners.m)}{9}{section.3.3}
\contentsline {section}{\numberline {3.4}Removing Outliers (ransac8point\_parallel.m)}{9}{section.3.4}
\contentsline {section}{\numberline {3.5}Get Landmarks (getLandmarks.m)}{10}{section.3.5}
\contentsline {section}{\numberline {3.6}Camera Position (ransacP3P\_pos\_cal\_parallel.m)}{10}{section.3.6}
\contentsline {chapter}{\numberline {4}Remarks}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Buffer}{11}{section.4.1}
\contentsline {section}{\numberline {4.2}Windowed Bundle Adjustment}{11}{section.4.2}
\contentsline {section}{\numberline {4.3}Relative Scale Factor}{11}{section.4.3}
