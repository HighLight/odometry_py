\chapter{Description of the most Important Functions}

\section{Creating the VO Interface (plotVO.m)}

A great effort has been invested into creating a computationally efficient visual odometry interface. Our basic outline of approach for creating the interface was, to not rely on standard MATLAB implementations such as \textit{plot} or \textit{scatter}, since such functions recreate various line- and axes-objects every time they are called, which is computationally expensive. Instead, our approach was to create all the required objects manually. This approach has the advantage, that all created objects such as axes-objects and line-objects can be stored in variables and if the VO operation requires to plot new results, the results can be visualized by just modifying the objects, instead of recreating them. This procedure saves computation time, however, the back draw of this approach is, that the dimensions of the data displayed by the objects have to be defined beforehand and can not vary during operation.\\

We started with creating a figure window and several axes-objects, while setting their parent to the created main figure window. Every axes-object needs a figure as parent (if the user does not specify any, MATLAB uses the last figure created or creates a new figure if needed). Our next step was to create an image-object with \textit{imshow}, while setting its parent to the corresponding axes-object. Every image-object needs an axes-object as parent (if the user does not specify any axes-object, matlab searches for the last one created, or creates a new one). The created image-object allows us to display the current image processed, by updating its CData (i. e.: $img\_object.CData = current\_image;$ ) without creating a new image-object.
For displaying the current frame number processed, we created a title object, that allows us to conveniently update its string property (i. e.: $title\_object.String = current\_string$).
Next, we created various line-objects (while setting their parent to the corresponding axes-object) that are needed to display our results after every frame processed. A line-object acts similar to an image-object. Every line-object also needs an axes-object as a parent (if the user does not set any axes-object, MATLAB uses the last axes created, or creates a new one). The created line-objects can be used to display our results by updating its XData (YData respectively), (i. e.: $line\_object.XData = points\_x;$ ) without creating a new line-object.\\

\section{Get Corners (getCorners.m)}

This function deals with getting corners from an image. We use MATLABs built in \textit{detectMinEigenFeatures} function, that uses the Shi-Tomasi algorithm to approximate the eigenvalues.
Further, we use MATLABs \textit{uniquetol} function. This function takes an array and keeps only one value per all similar values within a threshold. Adding the \textit{uniquetol} function has the advantage, that the corners are better distributed within the image. We noticed, that the initialization of landmarks from structure from motion (SFM) runs more stable if the corners are filtered with \textit{uniquetol}. As also stated in the \textit{Number of Features and Distribution} chapter at page 5 in the paper by Scaramuzza \textit{Visual Odometry Tutorial part I}: Equally distributed corners provide a more stable motion-estimation.\\

\section{Match Corners (matchCorners.m)}

Our \textit{matchCorners.m} function deals with \textit{matchingCorners.m} and updating matched corners in preceding images.

First, our \textit{matchCorners.m} function takes the corners from the second actual image and tracks the corners in the actual image using MATLABs built in \textit{KLT tracker}. 
Next, 8-point RANSAC is performed to remove outliers. Please find the \textit{8-point RANSAC} section for further reading on our 8-point RANSAC implementation. 
All corners in the second actual image and the actual image get deleted if they either are not found within the actual image or are determined to be outliers.
As a next step, all corners that are not found in the actual image are deleted for all the preceding frames (up to the last keyframe).
If landmarks already have been calculated, the function simultaneously also deletes the corresponding landmarks that belong to deleted corners.
As a next step, new corners are gathered from the image by calling the \textit{getCorners.m} function. Additionally, the distances between the matched corners and the new corners are calculated using MATLABs built in \textit{pdist2} function in order to only add corners that do not exist yet. A corner is considered nonexistent, if no corners exist within the radius of the variable \textit{min\_distance\_for\_existing\_corners.}\\

\section{Removing Outliers (ransac8point\_parallel.m)}

Quite some effort has been invested in creating vectorized statements within the RANSAC function. Since this function is executed over 1000 times each frame, every line is computationally very expensive. In order to further enhance efficiency, the for loop is parallelized by MATLABs standard \textit{parfor loop}. In a parallelized loop, each iteration has to be able to run independently of each other, in order to prevent race condition. This is achieved by only saving the number of inliers as also the indexes of the corners, that are used to calculate the inliers. In the end, the indexes of the particular parfor loop that yielded the most inliers is taken to calculate the inliers mask.

The inliers are determined by calculating the distance of every corner to its epipolar line. A vectorized statement has been written while using the Hesse formula for calculating the distance from a point to a line:

\begin{center}
$d = \dfrac{|a*x_0 + b*y_0 + c|}{\sqrt{a^2 + b^2}}$ 
\end{center}

Epipolar lines are calculated by $F*p1$. They are of the form $[a; b; c]$. While the points p2 are of the form $[x; y; 1]$.
The numerator can be executed in a vectorized statement as follows:\\

\textit{$abs(sum(matched\_corners2.*epipolar\_lines1,1));$}\\
 
Every point p2 gets calculated with its epipolar line from F and p1. The sum in dimension 1 calculates $a*x + b*y + 1*c$ for every point.

The denominator can be executed in a vectorized statement as follows:\\

\textit{$sqrt(sum(epipolar\_lines1(1:2,:).*epipolar\_lines1(1:2,:),1));$}\\

The values a and b of the epipolar lines get squared and the sum over dimension 1 is taken.

The following code snippet also calculates the numerator in a vectorized fashion. However, this code snippet has been found to be several times slower than the first version.\\

\textit{$abs(diag(matched\_corners2'*epipolar\_lines1));$}

\section{Get Landmarks (getLandmarks.m)}

The \textit{getLandmarks.m} function executes SFM from the exercises. A further constraint has been implemented for the corners to be met in order that landmarks get triangulated. The function checks, if the corners have enough displacement between the two frames, before triangulation is done with that corner. The calculation of the bearing angle has also been implemented and is executed, if camera positions are available. The bearing angle is calculated by taking the pixel coordinates of the corners, multiplying it with the inverse of the K matrix to get camera coordinates up to a scale factor and then multiplying them with the transposed rotation matrix of the M-matrix $M = [R\_C\_W , T\_C\_W]$:\\

\textit{$vector1 = R\_W\_C1 * inverse(K) * p1 = transpose(R\_C\_W1) * inverse(K) * p1$}\\

\textit{$vector2 = R\_W\_C2 * inverse(K) * p2 = transpose(R\_C\_W2) * inverse(K) * p2$}\\

The angle can be calculated by taking the dot product.\\
The threshold of the angle is determined by getting the 80 \% of the corners that yielded the largest bearing angle.\\
Additionally, the \textit{getLandmarks.m} function calculates the average depth of the landmarks. This value is needed to determine if the landmarks are safe enough to calculate the position of the camera.

\section{Camera Position (ransacP3P\_pos\_cal\_parallel.m)}

This function is based on the RANSAC p3p algorithm from the exercises. The for loop has been parallelized into a parfor loop using the same approach as in the 8-point RANSAC algorithm. In the end, the iteration that yielded the most inliers is taken to gather the indexes of the 3 points that were used to calculate the inliers. 

As stated in Scaramuzza's paper: \textit{P3P} is the standard procedure for motion estimation. This statement has led to the idea of modifying the \textit{RANSAC p3p} script as follows: To not calculate the camera position with all inliers using \textit{DLT}, but to calculate the position using the 3 points that have been used as a sample in the particular RANSAC iteration that yielded the most inliers.

In our opinion, this behavior can be compared to an averaging and a median filter. The \textit{DLT algorithm} tries to fit in a camera position with all inliers, whether they are good or not, while the \textit{p3p algorithm} only takes the 3 best points to calculate the camera position.










